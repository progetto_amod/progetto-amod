# AMOD Project 

This project is a Java implementation of Gomory cuts 
algorithm for solving integer programming Uncapacitated 
Facility Location (UFL) problem.

## Installation

### Requirements
* Java 1.8 
* Gurobi Optimizer software 8.1.0 

#### Gurobi License

Go to:
http://www.gurobi.com 
\
to download the target version of the optimizer and request a license (academic or commercial).

## Usage
* Run the Main.java class
* Choose the target UFL instance to solve
* Choose the Gomory cut type to generate
* Choose number of iterations 
* Choose a target directory to store ILP and LP solutions per iterations
* Solve the problem

## Results
The results window shows a chart with the objective function 
value for each linear iteration and an horizontal line for 
the integer one. 
Additionally it shows the general statistics for the algorithm:

* ILP and LP objective function value
* Integrity gap
* Total solve time for ILP and LP with gomory cuts
* Total solve time for each LP iteration
* Gomory cut type chosen for the algorithm

## Instances
We added five new instances to better test our system. 
In fact, _p1, p2, p5_ provided integer solutions to LP solving.
We used _test.txt_ instance to check if the constraints generation
was correct. Since _test1.txt_ did not provide integer solution to
the _relaxed_ problem, we used it for checking the correctness of the generated cuts.
Lastly, we used _p52.txt_ to test our system for a non-trivial instance.