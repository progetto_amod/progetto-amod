package main;

import gurobi.*;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
/*
    Fractional gomory cut class
 */
public class GomoryCutFract {
    private double beta; //constant term
    private GRBLinExpr expression; //left hand side of cut expression
}
