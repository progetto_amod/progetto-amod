package main;

import gurobi.*;
import lombok.Getter;
import org.ejml.simple.SimpleMatrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;

@Getter
public class UFLSolver {

    private ArrayList<Double> linearSolutions = new ArrayList<>(); //linear solutions per iteration
    private double integerSolution = 0.0; //ILP optimal solution
    private long totalSolveTime = 0; //total solve time for gomory cuts method
    private ArrayList<Long> iterationSolveTime = new ArrayList<>(); //solve time per iteration
    private int iterations; //effective numbers of iterations
    private long integerSolveTime = 0; //ILP solve time
    private String solutionDir;
    private CutType cutType;


    public void solve(String path, int iterations, CutType cutType, boolean vertical_read, String solutionDir) {
        Instance instance = new Instance(path, vertical_read);
        this.solutionDir = solutionDir;
        this.cutType = cutType;
        // Gurobi environment
        GRBEnv env = null;
        try {
            int ng = 0;
            env = new GRBEnv();

            solveIntegerModel(env, instance); //find ILP optimal solution

            // ------------------------- MODEL CREATION
            GRBModel model = new GRBModel(env);
            model.set(GRB.StringAttr.ModelName, "Uncapacitated facility location");

            // Facility open decision variables: open[f] == 1 if facility p is open.
            int nFacility = instance.getNFacility();
            int nCustomers = instance.getNCustomers();
            GRBVar[] open = new GRBVar[nFacility];
            for (int p = 0; p < nFacility; ++p) {
                open[p] = model.addVar(0, GRB.INFINITY, instance.getFixedOpenCost()[p], GRB.CONTINUOUS, "Open" + p);
            }

            // Service decision variables
            // x_i_j i: facility j:customer
            GRBVar[][] service = new GRBVar[nFacility][nCustomers];
            double[][] serviceCost = instance.getServiceCosts();
            for (int n = 0; n < nFacility; ++n) {
                for (int m = 0; m < nCustomers; ++m) {
                    service[n][m] =
                            model.addVar(0, GRB.INFINITY, serviceCost[n][m], GRB.CONTINUOUS,
                                    "Service" + n + "." + m);
                }
            }

            GRBVar[] slacks = new GRBVar[nCustomers * nFacility + nCustomers];
            for (int i = 0; i < slacks.length; i++)
                slacks[i] = model.addVar(0, GRB.INFINITY, 0, GRB.CONTINUOUS, "slack" + i);

            // The objective is to minimize the total fixed and variable costs
            model.set(GRB.IntAttr.ModelSense, GRB.MINIMIZE);

            // Demand constraints
            // Every customer gets service from at least one facility
            int s = 0;
            for (int w = 0; w < nCustomers; ++w) {
                GRBLinExpr dtot = new GRBLinExpr();
                for (int p = 0; p < nFacility; ++p) {
                    dtot.addTerm(1.0, service[p][w]);
                }
                dtot.addTerm(-1.0, slacks[s++]);
                model.addConstr(dtot, GRB.EQUAL, 1.0, "Demand" + w);
            }

            /*  Service constraints
                A customer cannot use the facility if it is closed
            */
            for (int p = 0; p < nFacility; ++p) {
                for (int w = 0; w < nCustomers; ++w) {
                    GRBLinExpr ptot = new GRBLinExpr();
                    ptot.addTerm(-1.0, service[p][w]);
                    ptot.addTerm(1.0, open[p]);
                    ptot.addTerm(-1.0, slacks[s++]);
                    model.addConstr(ptot, GRB.EQUAL, 0.0, "Openess" + p + "-" + w);
                }
            }
            model.set(GRB.IntParam.Method, 0);
            SimpleMatrix A = instance.getMatrix();
            SimpleMatrix b = instance.getConstantTerms();

            // Algorithm core
            int iteration;
            totalSolveTime = System.currentTimeMillis();
            for (iteration = 0; iteration < iterations; iteration++) {
                long partIterTime = System.currentTimeMillis();
                model.write("constraints" + iteration + ".lp"); //debug only
                model.optimize(); //solve linear problem

                //check whether linear solution is equal to integer one
                if (model.get(GRB.DoubleAttr.ObjVal) == this.integerSolution) {
                    linearSolutions.add(model.get(GRB.DoubleAttr.ObjVal));
                    break;
                }
                if (model.get(GRB.DoubleAttr.ObjVal) > this.integerSolution) {
                    break;
                }
                linearSolutions.add(model.get(GRB.DoubleAttr.ObjVal));
                //print solutions on file
                model.write(this.solutionDir+"/linear_solution_"+iteration+".sol");

                // extracting variables
                GRBVar[] vars = new GRBVar[(nFacility * nCustomers) + nFacility + slacks.length];
                int n = 0;
                for (int i = 0; i < nFacility; i++) {
                    for (int j = 0; j < nCustomers; j++) {
                        vars[n] = model.getVarByName("Service" + i + "." + j);
                        n++;
                    }
                }
                for (int j = 0; j < nFacility; j++) {
                    vars[n] = model.getVarByName("Open" + j);
                    n++;
                }

                for (int k = 0; k < slacks.length; k++) {
                    vars[n] = slacks[k];
                    n++;
                }

                // control if an integer solution is found
                if (areIntegers(vars)) {
                    System.out.println("Integer solution found");
                    break;
                }

                // compute B, N matrices
                Gomory gomory = new Gomory(vars, A, b, model.getConstrs(), nFacility, nCustomers);

                GRBVar newSlack;
                GRBLinExpr linExpr;
                boolean stop = false;
                // generate Gomory cut depending on specified cut mode
                switch (cutType) {
                    case SINGLE_INTEGER:
                        newSlack = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "slack" + slacks.length);
                        model.update();
                        slacks = (GRBVar[]) appendToVect(slacks, newSlack);
                        GomoryCut cut = gomory.generateSingleIntegerCut();
                        linExpr = cut.getExpression();
                        linExpr.addTerm(-1, newSlack);
                        model.addConstr(linExpr, GRB.EQUAL, cut.getBeta(), "gomory" + ng++);
                        b = appendToVector(b, cut.getBeta());
                        A = appendToMatrix(A, extractConstraintRow(linExpr, vars));
                        break;
                    case MULTIPLE_INTEGER:
                        Vector<GomoryCut> cuts = gomory.generateAllIntegerCut();
                        for (GomoryCut gomoryCut : cuts) {
                            newSlack = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "slack" + slacks.length);
                            model.update();
                            slacks = (GRBVar[]) appendToVect(slacks, newSlack);
                            linExpr = gomoryCut.getExpression();
                            linExpr.addTerm(-1, newSlack);
                            model.addConstr(linExpr, GRB.EQUAL, gomoryCut.getBeta(), "gomory" + ng++);
                            b = appendToVector(b, gomoryCut.getBeta());
                            A = appendToMatrix(A, extractConstraintRow(linExpr, vars));
                        }
                        break;
                    case SINGLE_FRACT:
                        newSlack = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "slack" + slacks.length);
                        model.update();
                        slacks = (GRBVar[]) appendToVect(slacks, newSlack);

                        GomoryCutFract g = gomory.generateSingleFractCut();
                        if (g == null) {
                            stop = true;
                            break;
                        }
                        linExpr = g.getExpression();
                        linExpr.addTerm(-1, newSlack);

                        model.addConstr(linExpr, GRB.EQUAL, g.getBeta(), "gomory" + ng++);
                        b = appendToVector(b, g.getBeta());
                        A = appendToMatrix(A, extractConstraintRow(linExpr, vars));
                        break;
                    case MULTIPLE_FRACT:
                        Vector<GomoryCutFract> gomoryCutVector = gomory.generateAllFractCut();
                        for (GomoryCutFract gomoryCut : gomoryCutVector) {
                            newSlack = model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "slack" + slacks.length);
                            model.update();

                            slacks = (GRBVar[]) appendToVect(slacks, newSlack);
                            linExpr = gomoryCut.getExpression();
                            linExpr.addTerm(-1, newSlack);
                            model.addConstr(linExpr, GRB.EQUAL, gomoryCut.getBeta(), "gomory" + ng++);
                            b = appendToVector(b, gomoryCut.getBeta());
                            A = appendToMatrix(A, extractConstraintRow(linExpr, vars));
                            vars = (GRBVar[]) appendToVect(vars, newSlack);
                        }
                        break;
                }
                if (stop) break;
                partIterTime = System.currentTimeMillis() - partIterTime;
                iterationSolveTime.add(iteration, partIterTime);
            }
            totalSolveTime = System.currentTimeMillis() - totalSolveTime;
            this.iterations = iteration + 1;
//            printSolution(model.getVars());
        } catch (GRBException e) {
            e.printStackTrace();
        }

    }


    //Generate and solve the ILP facility location problem
    private void solveIntegerModel(GRBEnv env, Instance instance) {
        try {
            GRBModel model = new GRBModel(env);
            model.set(GRB.StringAttr.ModelName, "Uncapacitated integer facility location");

            // Facility open decision variables: open[f] == 1 if facility p is open.
            int nFacility = instance.getNFacility();
            int nCustomers = instance.getNCustomers();
            GRBVar[] open = new GRBVar[nFacility];
            for (int p = 0; p < nFacility; ++p) {
                open[p] = model.addVar(0, 1, instance.getFixedOpenCost()[p], GRB.BINARY, "Open" + p);
            }

            // Service decision variables
            // x_i_j i: facility j:customer
            GRBVar[][] service = new GRBVar[nFacility][nCustomers];
            double[][] serviceCost = instance.getServiceCosts();
            for (int n = 0; n < nFacility; ++n) {
                for (int m = 0; m < nCustomers; ++m) {
                    service[n][m] =
                            model.addVar(0, 1, serviceCost[n][m], GRB.BINARY,
                                    "Service" + n + "." + m);
                }
            }

            // The objective is to minimize the total fixed and variable costs
            model.set(GRB.IntAttr.ModelSense, GRB.MINIMIZE);

            // Demand constraints
            int s = 0;
            for (int w = 0; w < nCustomers; ++w) {
                GRBLinExpr dtot = new GRBLinExpr();
                for (int p = 0; p < nFacility; ++p) {
                    dtot.addTerm(1.0, service[p][w]);
                }
                model.addConstr(dtot, GRB.GREATER_EQUAL, 1.0, "Demand" + w);
            }

            /*  Service constraints
                A customer cannot use the facility if it is closed
            */
            for (int p = 0; p < nFacility; ++p) {
                for (int w = 0; w < nCustomers; ++w) {
                    GRBLinExpr ptot = new GRBLinExpr();
                    ptot.addTerm(-1.0, service[p][w]);
                    ptot.addTerm(1.0, open[p]);
                    model.addConstr(ptot, GRB.GREATER_EQUAL, 0.0, "Openess" + p + "-" + w);
                }
            }
            model.set(GRB.IntParam.Method, 0);

            // ------------------
            System.out.println("\n\n-----------------Integer solution-----------------");
            long startTime = System.currentTimeMillis();
            model.optimize();
            integerSolveTime = System.currentTimeMillis() - startTime;
            System.out.println("-----------------End integer solution-----------------\n\n");
            integerSolution = model.get(GRB.DoubleAttr.ObjVal);
            model.write(this.solutionDir+"/ILP_solution.sol");

            //print solution on file
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


    private double[] extractConstraintRow(GRBLinExpr linExpr, GRBVar[] vars) {
        try {
            HashMap<String, Integer> variables = new HashMap<>();

            for (int i = 0; i < vars.length; i++)
                variables.put(vars[i].get(GRB.StringAttr.VarName), i);

            double[] row = new double[vars.length + 1];

            for (int i = 0; i < linExpr.size(); i++) {
                GRBVar var = linExpr.getVar(i);
                Integer index = variables.get(var.get(GRB.StringAttr.VarName));
                if (index != null)
                    row[index] = linExpr.getCoeff(i);
            }
            row[row.length - 1] = -1;

            return row;

        } catch (GRBException e) {
            e.printStackTrace();
            return null;
        }
    }

    //append the new gomory cuts line to A matrix
    private SimpleMatrix appendToMatrix(SimpleMatrix matrix, double[] row) {
        SimpleMatrix newMatrix = new SimpleMatrix(matrix.numRows() + 1, matrix.numCols() + 1);

        for (int i = 0; i < matrix.numRows(); i++) {
            for (int j = 0; j < matrix.numCols(); j++) {
                newMatrix.set(i, j, matrix.get(i, j));
            }
        }

        for (int i = 0; i < newMatrix.numRows(); i++)
            newMatrix.set(i, newMatrix.numCols() - 1, 0);

        for (int i = 0; i < newMatrix.numCols(); i++)
            newMatrix.set(newMatrix.numRows() - 1, i, row[i]);

        return newMatrix;
    }

    // Append a generic object to a static array
    private Object[] appendToVect(Object[] vect, Object o) {
        vect = Arrays.copyOf(vect, vect.length + 1);
        vect[vect.length - 1] = o;
        return vect;
    }

    private SimpleMatrix appendToVector(SimpleMatrix v, double val) {
        SimpleMatrix newVect = new SimpleMatrix(v.numRows() + 1, v.numCols());

        for (int i = 0; i < v.numRows(); i++)
            newVect.set(i, 0, v.get(i, 0));
        newVect.set(v.numRows(), 0, val);
        return newVect;
    }

    //check whether solution found in linear programming is integer or not
    private boolean areIntegers(GRBVar[] vars) {
        for (int i = 0; i < vars.length; i++) {
            try {
                if (vars[i].get(GRB.DoubleAttr.X) % 1 != 0.0)
                    return false;
            } catch (GRBException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

}