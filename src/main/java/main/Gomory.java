package main;

import lombok.*;
import gurobi.*;
import org.ejml.simple.SimpleMatrix;

import java.util.HashMap;
import java.util.Random;
import java.util.Vector;

@Getter
@Setter
public class Gomory {
    private GRBVar[] vars;
    private SimpleMatrix A;
    private SimpleMatrix B;
    private SimpleMatrix N;
    private SimpleMatrix constantTerms;
    private GRBVar[] xBase, xNotInBase;
    private SimpleMatrix bTilde;
    private SimpleMatrix alpha;
    private GRBConstr[] constrs;
    private int nFacility, nCustomers;

    public Gomory(GRBVar[] vars, SimpleMatrix a, SimpleMatrix constantTerms, GRBConstr[] constrs, int fac, int cus) {
        this.vars = vars;
        A = a;
        this.constrs = constrs;
        this.nFacility = fac;
        this.nCustomers = cus;
        this.constantTerms = constantTerms;
        try {
            //first, compute the N and B matrices
            computeNB();
            //then, compute the bTilde matrix, rounding the resulting terms
            // bTilde = B^(-1)*b
            bTilde = B.invert().mult(constantTerms);
            roundMatrix(bTilde);
            //alpha = B^(-1)*N
            alpha = B.invert().mult(N);
            //round the alpha terms
            roundMatrix(alpha);
        } catch (GRBException e) {
            e.printStackTrace();
        }
    }

    private void roundMatrix(SimpleMatrix matrix) {
        //Gurobi roundes the constant terms to 10^(-13), so we apply a rounding until this value
        for (int i = 0; i < matrix.numCols(); i++){
            for (int j = 0; j < matrix.numRows(); j++){
                matrix.set(j, i, (double)Math.round(matrix.get(j, i) * 1000000000000000d) / 1000000000000000d);
            }
        }
    }

    private void computeNB() throws GRBException {
        //Getting the dimensions of B, N from A
        this.B = new SimpleMatrix(A.numRows(), A.numRows());
        this.N = new SimpleMatrix(A.numRows(), A.numCols() - A.numRows());
        this.xBase = new GRBVar[A.numRows()];
        this.xNotInBase = new GRBVar[A.numCols() - A.numRows()];

        int j = 0, k = 0;

        HashMap<String, Integer> inBase = new HashMap<>();
        //we use a hashmap to keep values of the in base variables
        // VBasis gives us the status of a given variable in the current basis:
        //if VBasis = 0, the variable is in base
        for (int i = 0; i < vars.length; i++) {
            if (vars[i].get(GRB.IntAttr.VBasis) == 0) {
                inBase.put(vars[i].get(GRB.StringAttr.VarName), 0);
            }
        }
        if (inBase.size() < A.numRows()) {
            for (int i = 0; i < constrs.length; i++) {
                if (constrs[i].get(GRB.IntAttr.CBasis) == 0)
                    inBase.put(vars[i + nCustomers * nFacility + nFacility].get(GRB.StringAttr.VarName), 0);
            }
        }

        for (int i = 0; i < vars.length; i++) {
            if (inBase.get(vars[i].get(GRB.StringAttr.VarName)) == null) {
                //if variable is not in base, add its column to the N matrix
                for (int n = 0; n < A.numRows(); n++)
                    N.set(n, j, A.get(n, i));
                xNotInBase[j] = vars[i];
                j++;
            } else {
                //otherwise, add it to the N matrix
                for (int n = 0; n < A.numRows(); n++) {
                    B.set(n, k, A.get(n, i));
                }
                xBase[k] = vars[i];
                k++;
            }
        }
    }

    //this function finds the best cut using an heuristic technique
    //which chooses the constraint with the maximum fractionary part between all the bTilde values
    private int chooseBestCut() {
        int maxIndex = -1;
        double max = 0;
        for (int i = 0; i < bTilde.numRows(); i++) {
            double fract = Math.abs(bTilde.get(i, 0) % 1);
            if (fract > max) {
                if (validateRow(i)) {
                    max = fract;
                    maxIndex = i;
                }
            }
        }
        return maxIndex;
    }

    //generating a single fractionary cut
    public GomoryCutFract generateSingleFractCut() {
        GRBLinExpr cut = new GRBLinExpr();

        int maxIndex = chooseBestCut();

        if (maxIndex == -1) return null;
        for (int i = 0; i < xNotInBase.length; i++) {

            cut.addTerm(Math.abs(alpha.get(maxIndex, i) % 1), xNotInBase[i]);
        }


        return new GomoryCutFract(bTilde.get(maxIndex, 0) % 1, cut);
    }
    //generating all fractionary cuts from our constraints
    public Vector<GomoryCutFract> generateAllFractCut() {
        Vector<GomoryCutFract> cuts = new Vector<>();

        for (int j = 0; j < bTilde.numRows(); j++) {
            if ((bTilde.get(j, 0) % 1) != 0) {
                GRBLinExpr cut = new GRBLinExpr();
                for (int i = 0; i < xNotInBase.length; i++) {
                    cut.addTerm(Math.abs(alpha.get(j, i) % 1), xNotInBase[i]);
                }
                cuts.add(new GomoryCutFract(bTilde.get(j, 0) % 1, cut));
            }
        }
        return cuts;
    }
    //generating a single integer cut
    public GomoryCut generateSingleIntegerCut() throws GRBException {
        GRBLinExpr cut = new GRBLinExpr();


        int maxIndex = chooseBestCut();

        cut.addTerm(-1, xBase[maxIndex]);

        for (int i = 0; i < xNotInBase.length; i++) {
            cut.addTerm(-1 * (Math.floor(alpha.get(maxIndex, i))), xNotInBase[i]);
        }

        return new GomoryCut((int) Math.floor(-1 * bTilde.get(maxIndex, 0)), cut);
    }

    //generating all integer cuts from our constraints
    public Vector<GomoryCut> generateAllIntegerCut() {
        Vector<GomoryCut> cuts = new Vector<>();
        for (int j = 0; j < bTilde.numRows(); j++) {
            if (!validateRow(j)) continue;
            if ((bTilde.get(j, 0) % 1) != 0) {
                GRBLinExpr cut = new GRBLinExpr();
                cut.addTerm(1, xBase[j]);
                for (int i = 0; i < xNotInBase.length; i++) {
                    cut.addTerm(-1 * Math.floor(alpha.get(j, i)), xNotInBase[i]);
                }
                cuts.add(new GomoryCut((int) Math.floor(-1 * bTilde.get(j, 0)), cut));
            }
        }
        return cuts;
    }

    //finding if a row is suitable for our cuts in terms of Gurobi approximation
    private boolean validateRow(int row) {
        for (int i = 0; i < alpha.numCols(); i++) {
            if (Math.abs(alpha.get(row, i) % 1) >= 1 * Math.pow(10, -13)) {
                return true;
            }
        }
        return false;
    }
}
