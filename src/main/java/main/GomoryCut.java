package main;

import gurobi.*;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
/*
    Integer gomory cut class
 */
public class GomoryCut {
    private int beta; //constant term
    private GRBLinExpr expression; //left hand side expression
}
