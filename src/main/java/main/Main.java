package main;

import boundary.MainController;

public class Main {
    public static void main(String[] args) {
        System.getProperty("java.home");
        MainController main = MainController.getInstance();
        main.run();
    }

}
