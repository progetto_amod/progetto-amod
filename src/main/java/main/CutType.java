package main;

import lombok.Getter;

@Getter
public enum CutType {
    SINGLE_INTEGER("Single integer"),
    MULTIPLE_INTEGER("Multiple integer"),
    SINGLE_FRACT("Single fractional"),
    MULTIPLE_FRACT("Multiple fractional");

    private final String description;

    CutType(String value){
        description = value;
    }
}
