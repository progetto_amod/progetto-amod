package main;

import lombok.Getter;
import lombok.Setter;
import org.ejml.data.Matrix;
import org.ejml.simple.SimpleMatrix;

import java.io.*;

@Getter
@Setter
/*
   Specific problem instance class, generated from single file
 */
public class Instance {

    private int nFacility, nCustomers;
    private double[] fixedOpenCost;
    private double[][] serviceCosts;

    public Instance(String filename, boolean flag) {
        try {
            //bufferead reader to reading file
            BufferedReader b = new BufferedReader(new FileReader(new File(filename)));
            String readLine;
            String[] splitted;

            //read n customers and m facility value
            readLine = b.readLine();
            if (readLine != null) {
                splitted = readLine.split(" ");
                this.nFacility = Integer.parseInt(splitted[0]);
                this.nCustomers = Integer.parseInt(splitted[1]);
            }

            //read open fixed costs
            this.fixedOpenCost = new double[nFacility];
            for (int i = 0; i < nFacility; i++) {
                splitted = b.readLine().split(" ");
                this.fixedOpenCost[i] = Double.parseDouble(splitted[1]);
            }

            //ignore demand
            for (int i = 0; i < Math.ceil(((double) nCustomers) / ((double) nFacility)); i++)
                b.readLine();

            //read service costs
            this.serviceCosts = new double[nFacility][nCustomers];
            if (flag) {
                for (int i = 0; i < nCustomers; i++) {
                    splitted = b.readLine().trim().replaceAll(" +", " ").split(" ");
                    for (int k = 0; k < nFacility; k++) {
                        this.serviceCosts[k][i] = Double.parseDouble(splitted[k]);
                    }
                }
            } else {
                int rem = nCustomers % nFacility;
                for (int i = 0; i < nFacility; i++) {
                    double[] row = new double[nCustomers];
                    for (int k = 0; k < (Math.ceil(((double) nCustomers) / ((double) nFacility))); k++) {
                        splitted = b.readLine().trim().replaceAll(" +", " ").split(" ");
                        int line = Math.min(nFacility, nCustomers);
                        if (k == (Math.ceil(((double) nCustomers) / ((double) nFacility))) - 1 && rem != 0) {
                            line = rem;
                        }
                        for (int j = 0; j < line; j++) {
                            row[j + k * Math.min(nFacility, nCustomers)] = Double.parseDouble(splitted[j]);
                        }
                    }
                    this.serviceCosts[i] = row;

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.err.println("file not formatted correctly");
            System.exit(1);
        }
    }


    //create A matrix for the problem
    public SimpleMatrix getMatrix() {
        SimpleMatrix matrix = new SimpleMatrix((nFacility + 1) * nCustomers, nFacility * (nCustomers + 1) + (nFacility + 1) * nCustomers);

        //demand constraints
        for (int i = 0; i < nCustomers; i++) {
            for (int j = 0; j < nFacility; j++) {
                matrix.set(i, i + j * nCustomers, 1.0);
            }
        }

        //openess constraints
        int prod = nCustomers * nFacility;
        for (int i = 0; i < nCustomers * nFacility; i++) {
            matrix.set(i + nCustomers, i, -1.0);
            matrix.set(i + nCustomers, prod + (int) Math.floor(i / nCustomers), 1.0);
        }

        //slack constraints
        for (int i = 0; i < (nFacility + 1) * nCustomers; i++) {
            matrix.set(i, i + nFacility * (nCustomers + 1), -1);
        }
        return matrix;
    }

    //Return column of constant terms
    public SimpleMatrix getConstantTerms() {
        SimpleMatrix constantTerms = new SimpleMatrix(nCustomers * nFacility + nCustomers, 1);
        for (int i = 0; i < nCustomers; i++)
            constantTerms.set(i, 0, 1);
        for (int i = nCustomers; i < nCustomers * nFacility + nCustomers; i++)
            constantTerms.set(i, 0, 0);
        return constantTerms;
    }
}