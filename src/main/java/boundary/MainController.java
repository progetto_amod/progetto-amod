package boundary;

public class MainController {
    private static MainController mainControl = null;

    private MainController() {
    }

    public static MainController getInstance() {
        if (mainControl == null) {
            mainControl = new MainController();
        }
        return mainControl;
    }


    //Create the main window of the application
    public void run() {
        new MainWindow();

    }

}
