package boundary;

import main.CutType;
import main.UFLSolver;
import javax.swing.*;
import java.io.File;

public class MainWindow {

    private JLabel labelTitle;
    private JPanel panelExplorer;
    private JButton buttonFile;
    private JRadioButton rbSingleFract;
    private JRadioButton rbSingleInteger;
    private JRadioButton rbMultiInt;
    private JRadioButton rbMultiFract;
    private JSpinner spinnerIterations;
    private JButton buttonSolve;
    private JPanel parentPanel;
    private JLabel fileChosen;
    private JPanel fileSelectedPanel;
    private JButton chooseButton;
    private JLabel directoryChosen;

    private String filePath;
    private String directoryPath = null;
    private String fileName = null;


    public MainWindow() {
        //main window frame
        JFrame frame = new JFrame("Facility Location");
        frame.setContentPane(parentPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 300);
        frame.setResizable(true);
        frame.setVisible(true);

        //iteration spinner selector settings
        spinnerIterations.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));

        //radio button group configuration
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(rbSingleFract); buttonGroup.add(rbSingleInteger); buttonGroup.add(rbMultiInt); buttonGroup.add(rbMultiFract);
        rbSingleFract.setSelected(true);

        //listener on file chooser button
        buttonFile.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home") + System.getProperty("file.separator") + "Desktop"));
            int returnValue = fileChooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                filePath = selectedFile.getAbsolutePath(); //absolute path to chosen file
                fileChosen.setText(filePath);
                fileName = selectedFile.getName();
            }
            if (directoryChosen.getText().equals("No directory selected") || fileChosen.getText().equals("No file selected") || ((int) spinnerIterations.getValue()) <= 0 ) buttonSolve.setEnabled(false);
            else buttonSolve.setEnabled(true);
        });

        if (directoryChosen.getText().equals("No directory selected") || fileChosen.getText().equals("No file selected") || ((int) spinnerIterations.getValue()) <= 0 ) buttonSolve.setEnabled(false);
        else buttonSolve.setEnabled(true);

        spinnerIterations.addChangeListener(e -> {
            if (directoryChosen.getText().equals("No directory selected") || fileChosen.getText().equals("No file selected") || ((int) spinnerIterations.getValue()) <= 0 ) buttonSolve.setEnabled(false);
            else buttonSolve.setEnabled(true);
        });

        //listener on directory chooser button
        chooseButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home") + System.getProperty("file.separator") + "Desktop"));
            int returnValue = fileChooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                directoryPath = selectedFile.getAbsolutePath() + System.getProperty("file.separator");
                directoryChosen.setText(directoryPath);
            }
            if (directoryChosen.getText().equals("No directory selected") || fileChosen.getText().equals("No file selected") || ((int) spinnerIterations.getValue()) <= 0 ) buttonSolve.setEnabled(false);
            else buttonSolve.setEnabled(true);
        });

        //solve the problem with specified parameters
        buttonSolve.addActionListener(e -> {
            int iterations = (int) spinnerIterations.getValue();
            CutType cutSelected = null;
            if (rbSingleFract.isSelected()) cutSelected = CutType.SINGLE_FRACT;
            if (rbSingleInteger.isSelected()) cutSelected = CutType.SINGLE_INTEGER;
            if (rbMultiInt.isSelected()) cutSelected = CutType.MULTIPLE_INTEGER;
            if (rbMultiFract.isSelected()) cutSelected = CutType.MULTIPLE_FRACT;

            UFLSolver solver = new UFLSolver();
            solver.solve(filePath, iterations, cutSelected, true, directoryPath);
            new ResultWindow(solver);
            frame.setVisible(false);
            frame.dispose();
        });
    }
}
