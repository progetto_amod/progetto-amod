package boundary;

import main.UFLSolver;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.internal.series.MarkerSeries;
import org.knowm.xchart.style.markers.SeriesMarkers;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class ResultWindow {
    private JPanel parentPanel;
    private JPanel statsPanel;
    private JPanel chartPanel;
    private JList statList;
    private JScrollPane scrollPane;
    private JList timeStatsList;
    private JScrollPane timeStatsPane;
    private JPanel panel2;

    public ResultWindow(UFLSolver solver) {
        double integrityGap = (double) Math.round((solver.getIntegerSolution() / solver.getLinearSolutions().get(solver.getLinearSolutions().size() - 1)) * 100000d) / 100000d;
        String stats[] = {
                "Integer solution: " + solver.getIntegerSolution(),
                "Last linear solution: " + solver.getLinearSolutions().get(solver.getLinearSolutions().size() - 1),
                "Integrity Gap: " + integrityGap,
                "Total solve time (ILP): " + solver.getIntegerSolveTime() + " ms",
                "Total solve time (Linear): " + solver.getTotalSolveTime() + " ms",
                "Gomory cut used: " + solver.getCutType().getDescription()
        };

        //solve time per iterations vector
        String[] timeStatsIters = new String[solver.getIterationSolveTime().size()];
        for (int i = 0; i < solver.getIterationSolveTime().size(); i++) {
            timeStatsIters[i] = "Solve time for iteration " + (i) + ": " + solver.getIterationSolveTime().get(i);
        }

        JFrame frame = new JFrame();
        frame.setResizable(true);
        frame.setContentPane(parentPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        parentPanel.setLayout(new BoxLayout(parentPanel, BoxLayout.Y_AXIS));

        //add statistics to frame
        statList.setListData(stats);
        statList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        statList.setLayoutOrientation(JList.VERTICAL);
        statList.setVisibleRowCount(-1);

        timeStatsList.setListData(timeStatsIters);
        timeStatsList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        timeStatsList.setLayoutOrientation(JList.VERTICAL);
        timeStatsList.setVisibleRowCount(-1);

        // Create XY Chart to display solutions

        ArrayList<Double> iterationsNum = new ArrayList<>();
        for (int i = 0; i < solver.getLinearSolutions().size(); i++) {
            iterationsNum.add(((double) i));
        }
        ArrayList<Double> integerHorizontalLine = new ArrayList<>();
        for (int i = 0; i < iterationsNum.size(); i++) {
            integerHorizontalLine.add(solver.getIntegerSolution());
        }

        //create data set for linear solutions
        XYChart chart = QuickChart.getChart("Result Chart", "Iterations", "Objective Function Value", "Linear solutions", iterationsNum, solver.getLinearSolutions());

        XYSeries iterseries = chart.getSeriesMap().get("Linear solutions");
        iterseries.setMarker(SeriesMarkers.DIAMOND);

        //create horizontal line for integer solution
        XYSeries horizontal = chart.addSeries("Integer Solution", iterationsNum, integerHorizontalLine);
        horizontal.setMarker(SeriesMarkers.NONE);
        horizontal.setLineColor(Color.ORANGE);
        chartPanel.setLayout(new BoxLayout(chartPanel, BoxLayout.PAGE_AXIS));
        JPanel xChartPanel = new XChartPanel<>(chart);
        chartPanel.add(xChartPanel);
        chartPanel.validate();

        frame.add(statsPanel, BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setVisible(true);
    }
}